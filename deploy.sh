#!/usr/bin/env bash
# deploy.sh
#

USERNAME=keatonlao
REPOSITORYNAME=use-goldendict

# If a command fails then the deploy stops
set -e

printf "\033[0;32mDeploying updates to Gitee...\033[0m"

# ----------------------------------------------
# Functions
pushFun() {
    # Add changes to git.
    git add .
    # Commit changes.
    # msg="Update $(date +"[%x %T]")"
    msg="Initial commit"
    if [ -n "$*" ]; then
        msg="$*"
    fi
    git commit -m "$msg"
    # Push source and build repos.
    git push origin master
}
# ----------------------------------------------

echo -e "\n\033[0;31;1m+ $REPOSITORYNAME\033[0m"
pushFun


echo -e "\n\033[0;31;1mGitee Pages\033[0m \033[0;34mhttps://gitee.com/help/articles/4136#article-header0\033[0m"
echo -e "\033[0;33mPlease go to Gitee to update the site manually:\033[0m"
echo "  https://gitee.com/$USERNAME/$REPOSITORYNAME/pages"